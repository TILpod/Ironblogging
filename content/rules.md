---
title: "Regeln"
date: 2022-12-17T20:08:00+01:00
draft: false
---
Das TILpod Iron Blogging startete am 01.01.2022 und endet nach einem Jahr mit Option auf Fortsetzung.

Das Thema des Blogbeitrags ist nicht vorgegeben, insbesondere ist kein Bezug zu TILpod oder technischen Themen vorgeschrieben. 

Die Blogs müssen aber „privat“ sein, wer fürs Bloggen bezahlt wird, kann nicht teilnehmen.

Jede Teilnehmerin und jeder Teilnehmer schreibt einen Blogpost pro Monat.

Für jeden Monat, in der man nichts schreibt, muss ein Betrag von 5 Euro in die [gemeinsame Kasse](https://opencollective.com/tilpodironblogging) eingezahlt werden.

Türmen sich die Schulden zu 30 Euro auf, so scheidet die Teilnehmerin oder der Teilnehmer automatisch aus und kann später gegen Bezahlung der 30 Euro wieder einsteigen. So wird verhindert, dass die Teilnehmerinnen und Teilnehmer in den Ruin getrieben werden.

Dabei sollte ein Blogartikel mehr sein als Microblogging (X (Twitter), Mastodon) oder nicht nur eine Sammlung von Links enthalten. Im Zweifelsfall entscheidest Du selbst, ob Du Dich mit dem Beitrag in der Gruppe lächerlich machst oder lieber 5 Euro bezahlst. 😉

Automatisch erstellte Posts (z.B. Linklisten aus Delicious oder Zusammenfassungen aller eigenen/fremder Tweets/Toots, etc.) werden nicht gewertet.

Insgesamt gibt es vier Wochen “Urlaub” im Jahr, das heißt Wochen, für die man vorher ankündigen darf, dass man da offline sein und nichts schreiben wird.

Die Einnahmen werden für einen guten Zweck gespendet. Die Teilnehmer können jährlich ein Projekt oder eine Organisation als Empfänger zur Abstimmung vorschlagen.

