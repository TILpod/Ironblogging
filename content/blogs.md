---
title: "Teilnehmende Blogs"
date: 2021-01-09
draft: false
---

## Hier eine Liste der teilnehmenden Autorinnen und Autoren mit ihren Blogs:

| Autorin / Autor        | Blog                                                             | Feedadressen                                                          |
| ---------------------- | ---------------------------------------------------------------- | --------------------------------------------------------------------- |
| Jan Fader              | [Blog von Jan Fader](https://blog.faderweb.de)                   | [Feed](https://blog.faderweb.de/index.xml)                            |
| Dirk Deimeke           | [Dirks Logbuch](https://www.deimeke.net/dirk/blog/)              | [Feed](https://www.deimeke.net/dirk/blog/index.php?/feeds/index.rss2) |
| Michael Gisbers        | [Gisbers.de](https://gisbers.de)                                 | [Feed](https://gisbers.de/index.xml)                                  |
| Sujeevan Vijayakumaran | [Blog \| svij \| Sujeevan Vijayakumaran](https://svij.org/blog/) | [Feed](https://svij.org/blog/index.xml)                               |
| Felix Kronlage-Dammers | [fkr \| Musings](https://hazardous.org/blog)                     | [Feed](https://hazardous.org/feed/feed.xml)                           |
| Jens Kubieziel         | [Qbi's Weblog](https://kubieziel.de/blog/)                       | [Feed](https://kubieziel.de/blog/feeds/index.rss2)                    |
| Arthur Schiwon         | [Arthur acquaints](https://arthur-schiwon.de)                    | [Feed](https://arthur-schiwon.de/rss.xml)                             |

## Hier folgen ehemalige Teilnehmer am TILpod-Ironblogging:

| Autorin / Autor        | Blog                                                             | Feedadressen                                                          |
| ---------------------- | ---------------------------------------------------------------- | --------------------------------------------------------------------- |
| Hans-Peter Brügger     | [Hampas Blog](https://hpbyte.ch/)                                | [Feed](https://www.hpbyte.ch/index.php?/feeds/index.rss2)             |
| Thomas Hochstein       | [Netz - Rettung - Recht](https://netz-rettung-recht.de/)         | [Feed](https://netz-rettung-recht.de/feeds/index.rss2)                |

