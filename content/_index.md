---
title: "Startseite"
---

{{< lead >}}
Wir sind die Ironbloggerinnen und Ironblogger des [TILpod](https://tilpod.net).

Jede Person, die mitmachen möchte, ist herzlich willkommen.

Wir organisieren uns im Matrix-Raum [TILpod-Ironblogging](https://matrix.to/#/#TILpod-Ironblogging:matrix.org).

Diese Webseite wird via [Codeberg Pages](https://codeberg.page/) gehostet. Zugriff auf das Repository [TILpod/Ironblogging](https://codeberg.org/TILpod/Ironblogging) gibt es nach Meldung des Usernamens an Dirk (via Chat oder E-Mail).
{{< /lead >}}
