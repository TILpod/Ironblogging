# Repository für die TILpod-Ironblogger

## lokales Setup für die Entwicklung

- Hugo in Version >= 0.91.2+extended
- Checkout des Repos und Submodule

```bash
git clone https://codeberg.org/TILpod/Ironblogging.git test
# Alternativ: git clone ssh://git@codeberg.org/TILpod/Ironblogging.git test
git submodule init
git submodule update
```

- Checkout des Branches pages in Ordner public

```bash
git clone --branch pages https://codeberg.org/TILpod/Ironblogging.git public
# Alternativ: git clone --branch pages ssh://git@codeberg.org/TILpod/Ironblogging.git public
```
## Updates

```bash
git pull --all
# Sumodule aktualisieren
git submodule update --recursive --remote
```

## Vorgehen für Änderungen

- Anpassung im main
- hugo ausführen
- mit Aufruf "hugo serve" unter [http://localhost:1313/](http://localhost:1313/) testen
- mit Aufruf "hugo" bauen. Dabei werden die statischen Dateien in public abgelegt
- Commit im Ordner public in den Branch pages
